Vue.filter('currency', (valor) => {
  return `${valor} €`;
});
new Vue({
  el: 'main',
  data: {
    hola: 'hola mundo',
    user: 'juan',
    surname: 'Moreno',
    elements: ['uno', 'dos'],
    objects: {'uno': 'dos'},
    price: 20,
    oldprice: 20,
  },
  methods: {
    click(user) {
      console.log('click', user);
    },
    userCompleted2() {
      // return user + '-' + surame;
      console.log('entro userCompleted2');
      return `${this.user} - ${this.surname}`;
    }
  },
  computed: {
    userCompleted() {
      console.log('entro userCompleted');
      const user = this.user;
      return `${user} - ${this.surname}`;
    }
  }
})

new Vue({
  el: '#el2',
})